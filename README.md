## Continuous Repository for kubernetes using FluxCD
[About Flux](https://docs.fluxcd.io/en/latest/introduction.html, "About flux")

Flux is an open source tool on the lines of GitOps. Acheives releases via Git commits (Automated git->cluster synchronisation). 
- A custom resource definition 'helmrelease' to define a release
- flux.weave.works/automated: "true" / "false" and flux.weave.works/tag.chart-image: pattern can acheive automated releases. 
- Values for charts can be provided by number of ways. [Learn how to supply values for Helm charts](https://docs.fluxcd.io/en/latest/tutorials/get-started-helm.html "Supplying values")
- There are many image tag filtering options, most important ones are semver and glob. One approach could to tag the image with environment-* pattern so that tag would drive which image gets automatically deployed to which environment e.g. test-xxx, stagging-xxx, live-xxx etc. 
- you can define any k8s resource in yaml files, flux would deploy that. However we would deploy applications using helm charts.

## Install fluxctl

flux cli gives control over flux deployments.
> Get public key for the identity of flux, which would be used while commiting to flux repo
```
fluxctl identity --k8s-fwd-ns flux
```
> Force sync
```
fluxctl sync --k8s-fwd-ns flux
```
For more details [read here](https://docs.fluxcd.io/en/latest/references/fluxctl.html "Fluxctl")